/**
*@Author:
*Benjamin Liguz
*
*
*/

#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include "device_functions.h"
#include <cuda.h>
#include <stdio.h>
#include <stdint.h>
#include <time.h>
#include <windows.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <cmath>


#define TILE_DIM 32
#define BLOCK_ROWS 32


using namespace std;

cudaError_t checkWithCuda(int *h_niebieski, int *h_czerwony, int* h_zielony, int *h_niebieski1, int *h_czerwony1, int *h_zielony1, int rozmiar, int szerokosc, int wysokosc);

cudaError_t shareComputing(int *h_niebieski, int *h_czerwony, int* h_zielony, int *h_niebieski1, int *h_czerwony1, int *h_zielony1, int rozmiar, int szerokosc, int wysokosc);

__global__ void liczenieNaShared(int* in, int* out, int wysokosc, int szerokosc)
{
	__shared__ int daneNaShared[TILE_DIM + 2][TILE_DIM + 2];

	int x = blockIdx.x * TILE_DIM + threadIdx.x;
	int y = blockIdx.y * TILE_DIM + threadIdx.y;


	bool is_x_left = (threadIdx.x == 0), is_x_right = (threadIdx.x == TILE_DIM - 1);
	bool is_y_top = (threadIdx.y == 0), is_y_bottom = (threadIdx.y == TILE_DIM - 1);

	if (is_x_left)
		daneNaShared[threadIdx.x][threadIdx.y + 1] = 0;
	else if (is_x_right)
		daneNaShared[threadIdx.x + 2][threadIdx.y + 1] = 0;
	if (is_y_top) {
		daneNaShared[threadIdx.x + 1][threadIdx.y] = 0;
		if (is_x_left)
			daneNaShared[threadIdx.x][threadIdx.y] = 0;
		else if (is_x_right)
			daneNaShared[threadIdx.x + 2][threadIdx.y] = 0;
	}
	else if (is_y_bottom) {
		daneNaShared[threadIdx.x + 1][threadIdx.y + 2] = 0;
		if (is_x_right)
			daneNaShared[threadIdx.x + 2][threadIdx.y + 2] = 0;
		else if (is_x_left)
			daneNaShared[threadIdx.x][threadIdx.y + 2] = 0;
	}

	daneNaShared[threadIdx.x + 1][threadIdx.y + 1] = in[y*szerokosc + x];

	if (is_x_left && (x>0))
		daneNaShared[threadIdx.x][threadIdx.y + 1] = in[y*szerokosc + (x - 1)];
	else if (is_x_right && (x<szerokosc - 1))
		daneNaShared[threadIdx.x + 2][threadIdx.y + 1] = in[y*szerokosc + (x + 1)];
	if (is_y_top && (y>0)) {
		daneNaShared[threadIdx.x + 1][threadIdx.y] = in[(y - 1)*szerokosc + x];
		if (is_x_left)
			daneNaShared[threadIdx.x][threadIdx.y] = in[(y - 1)*szerokosc + (x - 1)];
		else if (is_x_right)
			daneNaShared[threadIdx.x + 2][threadIdx.y] = in[(y - 1)*szerokosc + (x + 1)];
	}
	else if (is_y_bottom && (y<wysokosc - 1)) {
		daneNaShared[threadIdx.x + 1][threadIdx.y + 2] = in[(y + 1)*szerokosc + x];
		if (is_x_right)
			daneNaShared[threadIdx.x + 2][threadIdx.y + 2] = in[(y + 1)*szerokosc + (x + 1)];
		else if (is_x_left)
			daneNaShared[threadIdx.x][threadIdx.y + 2] = in[(y + 1)*szerokosc + (x - 1)];
	}

	__syncthreads();


	out[y*szerokosc + x] = (daneNaShared[threadIdx.x][threadIdx.y] + daneNaShared[threadIdx.x + 1][threadIdx.y] + daneNaShared[threadIdx.x + 2][threadIdx.y]
		+ daneNaShared[threadIdx.x][threadIdx.y + 1] + daneNaShared[threadIdx.x + 1][threadIdx.y + 1] + daneNaShared[threadIdx.x + 2][threadIdx.y + 1] +
		daneNaShared[threadIdx.x][threadIdx.y + 2] + daneNaShared[threadIdx.x + 1][threadIdx.y + 2] + daneNaShared[threadIdx.x + 2][threadIdx.y + 2]) / 9;
}




__global__ void liczenieNaGlobal(int *dev_niebieski, int *dev_czerwony, int *dev_zielony, int* wynik_niebieski, int* wynik_czerwony, int* wynik_zielony, int szerokosc, int wysokosc)
{
	int i = blockIdx.x * blockDim.x + threadIdx.x;
	int j = blockIdx.y * blockDim.y + threadIdx.y;
	for (i; i < wysokosc - 1; i += blockDim.x * gridDim.x)
		for (j; j < szerokosc - 1; j += blockDim.y * gridDim.y){
			wynik_niebieski[i*szerokosc + j] = (1 * dev_niebieski[(i - 1)*szerokosc + j - 1] + 1 * dev_niebieski[i*szerokosc + j - 1] + 1 * dev_niebieski[(i + 1)*szerokosc + j - 1]
				+ 1 * dev_niebieski[(i - 1)*szerokosc + j] + 2 * dev_niebieski[i*szerokosc + j] + 1 * dev_niebieski[(i + 1)*szerokosc + j] + 1 * dev_niebieski[(i - 1)*szerokosc + j + 1]
				+ 1 * dev_niebieski[i*szerokosc + j + 1] + 1 * dev_niebieski[(i + 1)*szerokosc + j + 1]) / 10;

			wynik_czerwony[i*szerokosc + j] = (1 * dev_czerwony[(i - 1)*szerokosc + j - 1] + 1 * dev_czerwony[i*szerokosc + j - 1] + 1 * dev_czerwony[(i + 1)*szerokosc + j - 1]
				+ 1 * dev_czerwony[(i - 1)*szerokosc + j] + 2 * dev_czerwony[i*szerokosc + j] + 1 * dev_czerwony[(i + 1)*szerokosc + j] + 1 * dev_czerwony[(i - 1)*szerokosc + j + 1]
				+ 1 * dev_czerwony[i*szerokosc + j + 1] + 1 * dev_czerwony[(i + 1)*szerokosc + j + 1]) / 10;

			wynik_zielony[i*szerokosc + j] = (1 * dev_zielony[(i - 1)*szerokosc + j - 1] + 1 * dev_zielony[i*szerokosc + j - 1] + 1 * dev_zielony[(i + 1)*szerokosc + j - 1]
				+ 1 * dev_zielony[(i - 1)*szerokosc + j] + 2 * dev_zielony[i*szerokosc + j] + 1 * dev_zielony[(i + 1)*szerokosc + j] + 1 * dev_zielony[(i - 1)*szerokosc + j + 1]
				+ 1 * dev_zielony[i*szerokosc + j + 1] + 1 * dev_zielony[(i + 1)*szerokosc + j + 1]) / 10;	
	}
}

/* funkcja odczytuje nag��wek pliku BMP i zapisuje informacj� w polach struktury BITMAPFILEHEADER */
int odczytaj_BFH(ifstream &ifs, BITMAPFILEHEADER &bfh)
{
	ifs.read(reinterpret_cast<char*>(&bfh.bfType), 2);
	ifs.read(reinterpret_cast<char*>(&bfh.bfSize), 4);
	ifs.read(reinterpret_cast<char*>(&bfh.bfReserved1), 2);
	ifs.read(reinterpret_cast<char*>(&bfh.bfReserved2), 2);
	ifs.read(reinterpret_cast<char*>(&bfh.bfOffBits), 4);
	return ifs.tellg();
}


/* funkcja odczytuje nag��wek pliku BMP, zapisuje informacj� w polach struktury BITMAPINFOHEADER oraz zwraca bie��c� pozycj� kursora */
int odczytaj_BIH(ifstream &ifs, BITMAPINFOHEADER &bih)
{
	ifs.read(reinterpret_cast<char*>(&bih.biSize), 4);
	ifs.read(reinterpret_cast<char*>(&bih.biWidth), 4);
	ifs.read(reinterpret_cast<char*>(&bih.biHeight), 4);
	ifs.read(reinterpret_cast<char*>(&bih.biPlanes), 2);
	ifs.read(reinterpret_cast<char*>(&bih.biBitCount), 2);
	ifs.read(reinterpret_cast<char*>(&bih.biCompression), 4);
	ifs.read(reinterpret_cast<char*>(&bih.biSizeImage), 4);
	ifs.read(reinterpret_cast<char*>(&bih.biXPelsPerMeter), 4);
	ifs.read(reinterpret_cast<char*>(&bih.biYPelsPerMeter), 4);
	ifs.read(reinterpret_cast<char*>(&bih.biClrUsed), 4);
	ifs.read(reinterpret_cast<char*>(&bih.biClrImportant), 4);
	return ifs.tellg();
}


/* funkcja przepisuje kolejne kolejne bajty ze strumienia ifs do pami�ci i zwraca wska�nik do zapisanych danych */
unsigned char* odczytaj_dane_obrazu(ifstream& ifs, int rozmiar, int kursor)
{
	ifs.seekg(kursor, ios::beg);
	unsigned char* obraz = new unsigned char[rozmiar];
	ifs.read(reinterpret_cast<char*>(obraz), rozmiar);
	return obraz;
}


void dev_odczytaj_rgb(unsigned char* obraz, int *h_niebieski, int* h_zielony, int* h_czerwony, int rozmiar, int szerokosc, int wysokosc)
{
	int zerowe_bajty = rozmiar / wysokosc - 3 * szerokosc;
	int k = 0;
	for (int i = 0; i < wysokosc; i++)
	{
		for (int j = 0; j < szerokosc; j++)
		{
			h_niebieski[i*szerokosc + j] = obraz[k++];
			h_zielony[i*szerokosc + j] = obraz[k++];
			h_czerwony[i*szerokosc + j] = obraz[k++];
		}
		k += zerowe_bajty;
	}
}


void dev_konwolucja(int wysokosc, int szerokosc, int* h_niebieski, int* h_zielony, int* h_czerwony, int* h_niebieski1, int* h_zielony1, int* h_czerwony1)
{
	// do kraw�dzi koloru t�a, czyli bajtom b�d�cym w pierwszym rz�dzie oraz w pierwszej kolumnie, zostaje przypisany kolor czarny
	for (int i = 1; i < wysokosc - 1; i++)
	for (int j = 1; j < szerokosc - 1; j++)
	{
		h_niebieski1[i*szerokosc + j] = (1 * h_niebieski[(i - 1)*szerokosc + j - 1] + 1 * h_niebieski[i*szerokosc + j - 1] + 1 * h_niebieski[(i + 1)*szerokosc + j - 1]
			+ 1 * h_niebieski[(i - 1)*szerokosc + j] + 2 * h_niebieski[i*szerokosc + j] + 1 * h_niebieski[(i + 1)*szerokosc + j] + 1 * h_niebieski[(i - 1)*szerokosc + j + 1]
			+ 1 * h_niebieski[i*szerokosc + j + 1] + 1 * h_niebieski[(i + 1)*szerokosc + j + 1]) / 10;

		h_czerwony1[i*szerokosc + j] = (1 * h_czerwony[(i - 1)*szerokosc + j - 1] + 1 * h_czerwony[i*szerokosc + j - 1] + 1 * h_czerwony[(i + 1)*szerokosc + j - 1]
			+ 1 * h_czerwony[(i - 1)*szerokosc + j] + 2 * h_czerwony[i*szerokosc + j] + 1 * h_czerwony[(i + 1)*szerokosc + j] + 1 * h_czerwony[(i - 1)*szerokosc + j + 1]
			+ 1 * h_czerwony[i*szerokosc + j + 1] + 1 * h_czerwony[(i + 1)*szerokosc + j + 1]) / 10;

		h_zielony1[i*szerokosc + j] = (1 * h_zielony[(i - 1)*szerokosc + j - 1] + 1 * h_zielony[i*szerokosc + j - 1] + 1 * h_zielony[(i + 1)*szerokosc + j - 1]
			+ 1 * h_zielony[(i - 1)*szerokosc + j] + 2 * h_zielony[i*szerokosc + j] + 1 * h_zielony[(i + 1)*szerokosc + j] + 1 * h_zielony[(i - 1)*szerokosc + j + 1]
			+ 1 * h_zielony[i*szerokosc + j + 1] + 1 * h_zielony[(i + 1)*szerokosc + j + 1]) / 10;
	}
}


void dev_zapisanie(ofstream& ofs, int rozmiar, BITMAPFILEHEADER &bfh, BITMAPINFOHEADER &bih, int wysokosc, int szerokosc, int* h_niebieski1, int* h_zielony1, int* h_czerwony1)
{
	ofs.seekp(0);
	ofs.write(reinterpret_cast<char*>(&bfh.bfType), 2);
	ofs.write(reinterpret_cast<char*>(&bfh.bfSize), 4);
	ofs.write(reinterpret_cast<char*>(&bfh.bfReserved1), 2);
	ofs.write(reinterpret_cast<char*>(&bfh.bfReserved2), 2);
	ofs.write(reinterpret_cast<char*>(&bfh.bfOffBits), 4);
	ofs.write(reinterpret_cast<char*>(&bih.biSize), 4);
	ofs.write(reinterpret_cast<char*>(&bih.biWidth), 4);
	ofs.write(reinterpret_cast<char*>(&bih.biHeight), 4);
	ofs.write(reinterpret_cast<char*>(&bih.biPlanes), 2);
	ofs.write(reinterpret_cast<char*>(&bih.biBitCount), 2);
	ofs.write(reinterpret_cast<char*>(&bih.biCompression), 4);
	ofs.write(reinterpret_cast<char*>(&bih.biSizeImage), 4);
	ofs.write(reinterpret_cast<char*>(&bih.biXPelsPerMeter), 4);
	ofs.write(reinterpret_cast<char*>(&bih.biYPelsPerMeter), 4);
	ofs.write(reinterpret_cast<char*>(&bih.biClrUsed), 4);
	ofs.write(reinterpret_cast<char*>(&bih.biClrImportant), 4);

	int zerowe_bajty = rozmiar / wysokosc - 3 * szerokosc;
	unsigned char *pojedynczy_bajt_zerowy = new unsigned char[zerowe_bajty];

	for (int i = 0; i < zerowe_bajty; i++)
		pojedynczy_bajt_zerowy[i] = 0;

	for (int i = 0; i < wysokosc; i++)
	{
		for (int j = 0; j < szerokosc; j++)
		{
			ofs.write(reinterpret_cast<char*>(&h_niebieski1[i*szerokosc + j]), 1);
			ofs.write(reinterpret_cast<char*>(&h_zielony1[i*szerokosc + j]), 1);
			ofs.write(reinterpret_cast<char*>(&h_czerwony1[i*szerokosc + j]), 1);
		}
		ofs.write(reinterpret_cast<char*>(pojedynczy_bajt_zerowy), zerowe_bajty);
	}

	delete[] pojedynczy_bajt_zerowy;
}


int main()
{
	BITMAPFILEHEADER bfh;
	BITMAPINFOHEADER bih;
	clock_t time_start, time_stop;
	double timeCPU, timeGPU;

	char nazwa1[100];
	cout << "Podaj nazwe wczytywanego pliku: ";
	cin >> nazwa1;

	/* otwierany jest plik wejsciowy */
	ifstream plik(nazwa1, ios::binary);
	if (!plik)
	{
		cout << "Plik nie zostal otwarty";
		return 1;
	}

	odczytaj_BFH(plik, bfh);

	int kursor = odczytaj_BIH(plik, bih);
	int rozmiar = bfh.bfSize - bfh.bfOffBits;
	int wysokosc = bih.biHeight;
	int szerokosc = bih.biWidth;
	int wymiary = szerokosc*wysokosc;

	unsigned char* obraz = odczytaj_dane_obrazu(plik, rozmiar, kursor);

	int** niebieski = new int*[wysokosc];
	int** czerwony = new int*[wysokosc];
	int** zielony = new int*[wysokosc];
	int** p_niebieski1 = new int*[wysokosc];
	int** p_czerwony1 = new int*[wysokosc];
	int** p_zielony1 = new int*[wysokosc];

	int* h_niebieski = (int*)malloc(rozmiar*sizeof(int));
	int* h_czerwony = (int*)malloc(rozmiar*sizeof(int));
	int* h_zielony = (int*)malloc(rozmiar*sizeof(int));
	int* h_niebieski1 = (int*)malloc(rozmiar*sizeof(int));
	int* h_czerwony1 = (int*)malloc(rozmiar*sizeof(int));
	int* h_zielony1 = (int*)malloc(rozmiar*sizeof(int));

	dev_odczytaj_rgb(obraz, h_niebieski, h_zielony, h_czerwony, rozmiar, szerokosc, wysokosc);

	char nazwa2[100];
	cout << "Podaj nazwe pliku wyjsciowego: ";
	cin >> nazwa2;

	// otwierany jest plik wyjsciowy
	ofstream plik1(nazwa2, ios::binary);
	if (!plik1)
	{
		cout << "Plik nie zostal otwarty";
		return 1;
	}
	

	int obliczenia;
	cout << "Podaj, gdzie chcesz wykonac obliczenia : " << endl << "1 - CPU" << endl << "2 - GPU - pamiec globalna" << endl << "3 - pamiec wspoldzielona" << endl;
	cin >> obliczenia;
	switch (obliczenia)
	{
	case 1: time_start = clock();
		dev_konwolucja(wysokosc, szerokosc, h_niebieski, h_zielony, h_czerwony, h_niebieski1, h_zielony1, h_czerwony1);	
		time_stop = clock();
		dev_zapisanie(plik1, rozmiar, bfh, bih, wysokosc, szerokosc, h_niebieski1, h_zielony1, h_czerwony1);
		timeCPU = (double)(time_stop - time_start) / CLOCKS_PER_SEC;
		cout << "Czas pracy na CPU to: " << timeCPU << endl;
		break;
	case 2: time_start = clock();
		checkWithCuda(h_niebieski, h_czerwony, h_zielony, h_niebieski1, h_czerwony1, h_zielony1, rozmiar, szerokosc, wysokosc);
		time_stop = clock();
		dev_zapisanie(plik1, rozmiar, bfh, bih, wysokosc, szerokosc, h_niebieski1, h_zielony1, h_czerwony1);
		timeGPU = (double)(time_stop - time_start) / CLOCKS_PER_SEC;
		cout << "Czas pracy na GPU to: " << timeGPU << endl;
		break;
	case 3: time_start = clock();
		shareComputing(h_niebieski, h_czerwony, h_zielony, h_niebieski1, h_czerwony1, h_zielony1, rozmiar, szerokosc, wysokosc);
		time_stop = clock();
		dev_zapisanie(plik1, rozmiar, bfh, bih, wysokosc, szerokosc, h_niebieski1, h_zielony1, h_czerwony1);
		timeCPU = (double)(time_stop - time_start) / CLOCKS_PER_SEC;
		cout << "Czas pracy na CPU to: " << timeCPU << endl;
		break;
	default:
	break;
	}
	
	plik.close();
	plik1.close();

	system("PAUSE");
	return 0;
}

cudaError_t checkWithCuda(int *h_niebieski, int *h_czerwony, int* h_zielony, int *h_niebieski1, int *h_czerwony1, int *h_zielony1, int rozmiar, int szerokosc, int wysokosc)
{
	int* dev_niebieski;
	int* dev_czerwony;
	int* dev_zielony;
	int* wynik_niebieski;
	int* wynik_czerwony;
	int* wynik_zielony;

	cudaError_t cudaStatus;
	cudaStatus = cudaSetDevice(0);

	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaSetDevice failed!");
		goto Error;
	}

	// alokacja
	cudaStatus = cudaMalloc((void**)&dev_niebieski, sizeof(int)*rozmiar);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMalloc failed!");
		goto Error;
	}

	cudaStatus = cudaMalloc((void**)&dev_czerwony, sizeof(int)*rozmiar);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMalloc failed!");
		goto Error;
	}

	cudaStatus = cudaMalloc((void**)&dev_zielony, sizeof(int)*rozmiar);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMalloc failed!");
		goto Error;
	}

	cudaStatus = cudaMalloc((void**)&wynik_czerwony, sizeof(int)*rozmiar);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMalloc failed!");
		goto Error;
	}

	cudaStatus = cudaMalloc((void**)&wynik_niebieski, sizeof(int)*rozmiar);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMalloc failed!");
		goto Error;
	}

	cudaStatus = cudaMalloc((void**)&wynik_zielony, sizeof(int)*rozmiar);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMalloc failed!");
		goto Error;
	}

	// kopiowanie danych na GPU
	cudaStatus = cudaMemcpy(dev_niebieski, h_niebieski, rozmiar*sizeof(int), cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMemcpy failed! Niebieski");
		goto Error;
	}

	cudaStatus = cudaMemcpy(dev_czerwony, h_czerwony, rozmiar*sizeof(int), cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMemcpy failed! Czerwony");
		goto Error;
	}

	cudaStatus = cudaMemcpy(dev_zielony, h_zielony, rozmiar*sizeof(int), cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMemcpy failed! Zielony");
		goto Error;
	}

	cudaStatus = cudaMemcpy(wynik_niebieski, h_niebieski1, rozmiar*sizeof(int), cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMemcpy failed! Niebieski");
		goto Error;
	}

	cudaStatus = cudaMemcpy(wynik_czerwony, h_czerwony1, rozmiar*sizeof(int), cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMemcpy failed! Czerwony");
		goto Error;
	}

	cudaStatus = cudaMemcpy(wynik_zielony, h_zielony1, rozmiar*sizeof(int), cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMemcpy failed! Zielony");
		goto Error;
	}
	//================================================odpalanie kernela===========================================	
	dim3 grid((szerokosc + 32 - 1) / 32, (wysokosc + 32 - 1) / 32);
	dim3 block(32, 32);
	liczenieNaGlobal << < grid, block >> >(dev_niebieski, dev_czerwony, dev_zielony, wynik_niebieski, wynik_czerwony, wynik_zielony, szerokosc, wysokosc);
	//============================================================================================================
	cudaThreadSynchronize();

	cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "Global launch failed: %s\n", cudaGetErrorString(cudaStatus));
		goto Error;
	}


	cudaStatus = cudaDeviceSynchronize();
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaDeviceSynchronize returned error code %d after launching Global!\n", cudaStatus);
		goto Error;
	}

	// Copy output vector from GPU buffer to host memory.
	cudaStatus = cudaMemcpy(h_niebieski1, wynik_niebieski, rozmiar*sizeof(int), cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMemcpy failed! Color blue");
		goto Error;
	}

	cudaStatus = cudaMemcpy(h_czerwony1, wynik_czerwony, rozmiar*sizeof(int), cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMemcpy failed! Color red");
		goto Error;
	}

	cudaStatus = cudaMemcpy(h_zielony1, wynik_zielony, rozmiar*sizeof(int), cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMemcpy failed! Color green");
		goto Error;
	}
Error:
	cudaFree(dev_niebieski);
	cudaFree(dev_czerwony);
	cudaFree(dev_zielony);
	cudaFree(wynik_niebieski);
	cudaFree(wynik_czerwony);
	cudaFree(wynik_zielony);

	return cudaStatus;
}


cudaError_t shareComputing(int *h_niebieski, int *h_czerwony, int* h_zielony, int *h_niebieski1, int *h_czerwony1, int* h_zielony1, int rozmiar, int szerokosc, int wysokosc)
{
	int* dev_niebieski;
	int* dev_czerwony;
	int* dev_zielony;
	int* wynik_niebieski;
	int* wynik_czerwony;
	int* wynik_zielony;

	cudaError_t cudaStatus;
	cudaStatus = cudaSetDevice(0);

	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaSetDevice failed!");
		goto Error;
	}

	// alokacja
	cudaStatus = cudaMalloc((void**)&dev_niebieski, sizeof(int)*rozmiar);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMalloc failed!");
		goto Error;
	}

	cudaStatus = cudaMalloc((void**)&dev_czerwony, sizeof(int)*rozmiar);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMalloc failed!");
		goto Error;
	}

	cudaStatus = cudaMalloc((void**)&dev_zielony, sizeof(int)*rozmiar);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMalloc failed!");
		goto Error;
	}

	cudaStatus = cudaMalloc((void**)&wynik_czerwony, sizeof(int)*rozmiar);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMalloc failed!");
		goto Error;
	}

	cudaStatus = cudaMalloc((void**)&wynik_niebieski, sizeof(int)*rozmiar);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMalloc failed!");
		goto Error;
	}

	cudaStatus = cudaMalloc((void**)&wynik_zielony, sizeof(int)*rozmiar);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMalloc failed!");
		goto Error;
	}

	// kopiowanie danych na GPU
	cudaStatus = cudaMemcpy(dev_niebieski, h_niebieski, rozmiar*sizeof(int), cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMemcpy failed! Niebieski");
		goto Error;
	}

	cudaStatus = cudaMemcpy(dev_czerwony, h_czerwony, rozmiar*sizeof(int), cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMemcpy failed! Czerwony");
		goto Error;
	}

	cudaStatus = cudaMemcpy(dev_zielony, h_zielony, rozmiar*sizeof(int), cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMemcpy failed! Zielony");
		goto Error;
	}

	cudaStatus = cudaMemcpy(wynik_niebieski, h_niebieski1, rozmiar*sizeof(int), cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMemcpy failed! Niebieski");
		goto Error;
	}

	cudaStatus = cudaMemcpy(wynik_czerwony, h_czerwony1, rozmiar*sizeof(int), cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMemcpy failed! Czerwony");
		goto Error;
	}

	cudaStatus = cudaMemcpy(wynik_zielony, h_zielony1, rozmiar*sizeof(int), cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMemcpy failed! Zielony");
		goto Error;
	}
	//================================================odpalanie kernela===========================================	
	dim3 grid((szerokosc + 32 - 1) / 32, (wysokosc + 32 - 1) / 32);
	dim3 block(32, 32);
	liczenieNaShared << < grid, block >> >(dev_niebieski, wynik_niebieski, wysokosc, szerokosc);
	liczenieNaShared << < grid, block >> >(dev_czerwony, wynik_czerwony, wysokosc, szerokosc);
	liczenieNaShared << < grid, block >> >(dev_zielony, wynik_zielony, wysokosc, szerokosc);
	//============================================================================================================
	cudaThreadSynchronize();

	cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "Shared launch failed: %s\n", cudaGetErrorString(cudaStatus));
		goto Error;
	}


	cudaStatus = cudaDeviceSynchronize();
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaDeviceSynchronize returned error code %d after launching Shared!\n", cudaStatus);
		goto Error;
	}

	// Copy output vector from GPU buffer to host memory.
	cudaStatus = cudaMemcpy(h_niebieski1, wynik_niebieski, rozmiar*sizeof(int), cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMemcpy failed! Color blue");
		goto Error;
	}

	cudaStatus = cudaMemcpy(h_czerwony1, wynik_czerwony, rozmiar*sizeof(int), cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMemcpy failed! Color red");
		goto Error;
	}

	cudaStatus = cudaMemcpy(h_zielony1, wynik_zielony, rozmiar*sizeof(int), cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMemcpy failed! Color green");
		goto Error;
	}
Error:
	cudaFree(dev_niebieski);
	cudaFree(dev_czerwony);
	cudaFree(dev_zielony);
	cudaFree(wynik_niebieski);
	cudaFree(wynik_czerwony);
	cudaFree(wynik_zielony);

	return cudaStatus;
}